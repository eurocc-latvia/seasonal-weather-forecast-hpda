import os
# os.environ["PROJ_LIB"] = "C:/Users/Tija/Anaconda3/Library/share"
import numpy as np
import xarray as xr
import netCDF4
import matplotlib.pyplot as plt

import datetime
from mpl_toolkits.basemap import Basemap
import scipy.interpolate
from statsmodels.distributions.empirical_distribution import ECDF
from scipy import interpolate
from joblib import Parallel, delayed
import time
# needs to be added to the environment
import pathlib
from geopy import distance
import pandas as pd
import seaborn as sns
#
import pickle

# Defining global variables
# let's initialize some variables that will be used throughout the script
# We will work within specific longitude and latitude, let's define them here
lonmin = 20
lonmax = 30
latmin = 55
latmax = 60

# for plots we need to tell the code what the map should look like - what part of the world should be plotted
# intialize a basemap that can be used in all functions
uerra_map = Basemap(llcrnrlon=lonmin, llcrnrlat=latmin, urcrnrlon=lonmax, urcrnrlat=latmax,
                    resolution='h',area_thresh=1000.,projection='lcc',
                    lat_1=48.,lat_2=48.,lon_0=8)

# Projection: Lambert Conformal Conic
# Central meridian: 8
# Standard parallel 1: 48
# Standard parallel 2: 48
# Latitude of origin: 48

#
def main_script():

    # The forecast that we need to downscale is
    forecast_year = 2021
    forecast_init_month = 5
    forecast_valid_month = 5

    # forecast_year = 2019
    # forecast_init_month = 1
    # forecast_valid_month = 1

    # let's define a point of interest
    poi_lat = 56.926998
    poi_lon = 24.329950

    # This variable can be changed to false, if the reference dataset for the specific VALID month is already craeted
    # if this is done once for January, it is valid for all other months of January if the reference period is unchanged
    create_reference = False

    # Downscaling are being done to a reference period that can be specified here
    reference_year_start = 1993
    reference_year_end = 2018

    # HPC
    # All directory variables should end with "/"
    uerra_ref_dir = "/home/sile/seasonal_prediction/data/UERRA_ref/" # directory for saving the UERRA reference (intermediate) results.
                                                                    # Make sure that you have "w" access to this folder

    uerra_dir = "/home/sile/seasonal_prediction/data/uerra_ecmwf/" # directory where the UEARRA data can be found
    forecast_directory = "/home/sile/seasonal_prediction/data/forecast/" # directory where the forecast data can be found

    output_directory = "/home/sile/seasonal_prediction/results/" # directory were the results will be saved, it will be created if it does not exists
    parallel_num = 8 # number of threads for parallelization

    # # HOME
    # uerra_ref_dir = "C:/data/seasonal_prediction/data/UERRA_ref/" # directory for saving the UERRA reference (intermediate) results.
    # # Make sure that you have "w" access to this folder
    #
    # uerra_dir = "C:/data/seasonal_prediction/data/uerra_ecmwf/" # directory where the UEARRA data can be found
    # forecast_directory = "C:/data/seasonal_prediction/data/" # directory where the forecast data can be found
    #
    # output_directory = "C:/data/seasonal_prediction/results/" # directory were the results will be saved, it will be created if it does not exists
    # parallel_num = 4



    seasonal_downscaling(uerra_dir, uerra_ref_dir, create_reference,
                         forecast_directory,forecast_year,
                         forecast_init_month, forecast_valid_month, reference_year_start, reference_year_end,
                         poi_lat, poi_lon, output_directory, parallel_num)

def plot_examples():
    # This is a test function to plot an UERRA data example and forecast data example
    # It is not necessary for normal functioning of the code, but it can be easily adapted in case of any problems with data
    directory = "D:/seasonal_prediction/model_data"
    file_name_uerra = directory + "/" + 'UERRA/UERRA_2017_01.nc'

    nc = netCDF4.Dataset(file_name_uerra)
    t2m_ue = nc['t2m']
    t2m_ue = np.array(t2m_ue)
    lon_ue = nc['longitude']
    lon_ue = np.array(lon_ue)
    lat_ue = nc['latitude']
    lat_ue = np.array(lat_ue)
    fig = plt.figure()

    map = Basemap(llcrnrlon=20, llcrnrlat=53.5, urcrnrlon=30, urcrnrlat=60, resolution='h', epsg=2494,  area_thresh=100.)
    t2m_ue = np.mean(t2m_ue, axis=0)
    levels = np.arange(-10,5,0.5)
    map.contourf(lon_ue, lat_ue, t2m_ue-273.16, latlon=True, levels=levels, extend='both', cmap=plt.cm.RdBu_r)
    map.drawcoastlines(linewidth=0.5)
    map.drawcountries(linewidth=0.5)
    # map.colorbar()
    # os.chdir("E:/post_doc/soil_moisture/figures_test")
    fig.savefig("test_uerra.png", dpi=300, bbox_inches='tight')

    ####

    file_name_fc = directory + "/" + 'download_test_1.nc'
    xr_in = xr.open_dataset(file_name_fc)
    time_start = datetime.datetime(2017, 1, 1)
    time_end = datetime.datetime(2017, 2, 1)
    t2m = xr_in.t2m.sel(time=slice(time_start, time_end))
    t2m = t2m.mean(dim='time')
    t2m = t2m.mean(dim='number')
    t2m_fc = np.array(t2m)
    lon_fc = np.array(xr_in.longitude)
    lat_fc = np.array(xr_in.latitude)
    lon_fc, lat_fc = np.meshgrid(lon_fc, lat_fc)

    levels = np.arange(-10.5,0,0.5)
    # levels = None
    fig = plt.figure()

    map = Basemap(llcrnrlon=20, llcrnrlat=53.5, urcrnrlon=30, urcrnrlat=60, resolution='h', epsg=2494,  area_thresh=100.)
    map.contourf(lon_fc, lat_fc, t2m_fc-273.16, latlon=True, levels=levels, extend='both', cmap=plt.cm.RdBu_r)
    map.drawcoastlines(linewidth=0.5)
    map.drawcountries(linewidth=0.5)
    # map.colorbar()

    fig.savefig("test_forecast.png", dpi=300, bbox_inches='tight')




def plot_forecast_example():
    # This is a test function to plot a forecast example
    # It is not necessary for normal functioning of the code, but it can be easily adapted in case of any problems with data
    directory = "D:/seasonal_prediction/model_data"
    file_name_anom = directory + "/" + "type_fcmean.nc"
    # file_test = directory + "/" + "download_test_1.nc"
    nc = netCDF4.Dataset(file_name_anom)

    # print(nc.variables)

    xr_in = xr.open_dataset(file_name_anom)
    time_start = datetime.datetime(2021, 3, 1)
    time_end = datetime.datetime(2021, 6, 1)
    t2a = xr_in.t2a.sel(time=slice(time_start, time_end))
    t2a = t2a.mean(dim='time')
    t2a = t2a.mean(dim='number')
    t2a_fc = np.array(t2a)
    lon_fc = np.array(xr_in.longitude)
    lat_fc = np.array(xr_in.latitude)
    lon_fc, lat_fc = np.meshgrid(lon_fc, lat_fc)

    # levels = np.arange(-10.5,0,0.5)
    levels = None
    fig = plt.figure()

    map = Basemap(llcrnrlon=20, llcrnrlat=53.5, urcrnrlon=30, urcrnrlat=59, resolution='h', epsg=2494,  area_thresh=100.)
    map.pcolormesh(lon_fc, lat_fc, t2a_fc, latlon=True,  cmap=plt.cm.OrRd, shading='auto')
    # map.contorf(lon_fc, lat_fc, t2a_fc, latlon=True, levels=levels, extend='both', cmap=plt.cm.OrRd_r)
    map.drawcoastlines(linewidth=0.5)
    map.drawcountries(linewidth=0.5)
    map.colorbar()

    fig.savefig("forecast_21_APR.png", dpi=300, bbox_inches='tight')



def open_grib_file():
    # This is a test function to test whether it is possible to open a grib file
    # It is not necessary for normal functioning of the code, but it can be easily adapted in case of any problems with data

    directory = "C:/data/seasonal_prediction/data/"
    file_name = "ecmwf_seas_forecast_2019_01_.grb"
    xr_in = xr.open_dataset(directory + file_name, engine='cfgrib')  # {'time': 48, 'height': 3, 'south_north': 100, 'west_east': 100}
    print(xr_in)



def get_uearra_data(uerra_dir, year, month):
    # This function reads UERRA data if they are downloaded as netCDF4 files
    # uerra_dir = "C:/data/seasonal_prediction/data/UERRA/"
    year_str = str(year)
    month_str = "{:02d}".format(month)
    # file_name = "ecmwf_seas_forecast_" + year_str + "_" +month_str  + "_.grb"
    uerra_fname = "UERRA_"+  year_str + "_" +month_str  + ".nc"
    xr_uerra = xr.open_dataset(uerra_dir + uerra_fname)
    lat = xr_uerra.latitude
    long = xr_uerra.longitude
    subset = (lat > latmin) & (lat < latmax) & (long > lonmin) & (long < lonmax)

    x_subset = np.where(subset.sum(dim='y') >0) # We want a rectangle in (y,x) coordinates
    y_subset = np.where(subset.sum(dim='x') >0)
    x_subset = np.squeeze(np.array(x_subset))
    y_subset = np.squeeze(np.array(y_subset))
    xr_uerra = xr_uerra.sel(x=x_subset, y=y_subset)
    # uerra_lat = np.array(xr_uerra.latitude)
    # uerra_lon = np.array(xr_uerra.longitude)
    return xr_uerra


def get_uerra_data_v2(uerra_dir, year, month):

    year_str = str(year)
    month_str = "{:02d}".format(month)
    # file_name = "ecmwf_seas_forecast_" + year_str + "_" +month_str  + "_.grb"
    # uerra_fname = "UERRA_"+  year_str + "_" +month_str  + ".nc"
    uerra_fname = "temperature_2m_all_"+ year_str  +month_str+".grb"
    # xr_uerra = xr.open_dataset(uerra_dir + uerra_fname, engine='cfgrib', chunks={"step": 10, "time": 200, "x":100,"y":100})
    xr_uerra = xr.load_dataset(uerra_dir + uerra_fname, engine='cfgrib')
    # xr_uerra = xr.open_dataset(uerra_dir + uerra_fname, engine='cfgrib')
    lat = xr_uerra.latitude
    long = xr_uerra.longitude
    subset = (lat > latmin) & (lat < latmax) & (long > lonmin) & (long < lonmax)

    x_subset = np.where(subset.sum(dim='y') >0) # We want a rectangle in (y,x) coordinates
    y_subset = np.where(subset.sum(dim='x') >0)
    x_subset = np.squeeze(np.array(x_subset))
    y_subset = np.squeeze(np.array(y_subset))
    xr_uerra = xr_uerra.sel(x=x_subset, y=y_subset)
    # uerra_lat = np.array(xr_uerra.latitude)
    # uerra_lon = np.array(xr_uerra.longitude)
    return xr_uerra


def get_forecast_data(forecast_dir, year, month):
    model_directory = forecast_dir
    year_str = str(year)
    month_str = "{:02d}".format(month)
    file_name = "ecmwf_seas_forecast_" + year_str + "_" +month_str  + "_.grb"
    xr_in = xr.open_dataset(model_directory + file_name, engine='cfgrib')
    xr_in.load()
    return xr_in

def interpolate_to_reference_xr(xr_in,lat_frc, lon_frc, ref_lat, ref_lon):
    # we can do the interpolation in lat/lon where the forecast grid is rectangular
    # lat_frc = np.array(xr_in.latitude)
    # lon_frc = np.array(xr_in.longitude)
    data_frc = np.array(xr_in.t2m)

    number_of_models = xr_in.dims['number']
    number_of_time_steps = xr_in.dims['step']
    frc_time = xr_in.valid_time
    # now we flip the latitude to be in ascending order
    lat_frc = np.flip(lat_frc)
    data_frc = np.flip(data_frc, axis=2)
    result_arr = np.zeros((number_of_models, number_of_time_steps, np.shape(ref_lat)[0], np.shape(ref_lat)[1]))
    for i in np.arange(number_of_models):
        for j in np.arange(number_of_time_steps):
            interpolator = scipy.interpolate.RegularGridInterpolator((lat_frc, lon_frc), data_frc[i,j,:,:],
                                                                     bounds_error=False, fill_value=np.nan)
            result_arr[i,j,:,:] = interpolator((ref_lat, ref_lon))
    return result_arr

def interpolate_to_reference_np(data_frc,lat_frc, lon_frc, ref_lat, ref_lon):
    # we can do the interpolation in lat/lon where the forecast grid is rectangular
    # lat_frc = np.array(xr_in.latitude)
    # lon_frc = np.array(xr_in.longitude)
    # data_frc = np.array(xr_in.t2m)

    number_of_models = np.shape(data_frc)[0]
    # number_of_time_steps = xr_in.dims['step']
    # frc_time = xr_in.valid_time
    # now we flip the latitude to be in ascending order
    lat_frc = np.flip(lat_frc)
    data_frc = np.flip(data_frc, axis=1)
    result_arr = np.zeros((number_of_models, np.shape(ref_lat)[0], np.shape(ref_lat)[1]))
    for i in np.arange(number_of_models):
        if i%10000 ==0:
            print("Progress of interpolation = ")
            print(i/number_of_models)
        interpolator = scipy.interpolate.RegularGridInterpolator((lat_frc, lon_frc), data_frc[i,:,:],
                                                                     bounds_error=False, fill_value=np.nan)
        result_arr[i,:,:] = interpolator((ref_lat, ref_lon))
    return result_arr

def do_downscaling(reference, frcst_ref, forecast):
    # all should be on the same grid
    n_x = np.shape(reference)[1]
    n_y = np.shape(reference)[2]
    # n_x = reference.dims['y'] # 62
    # n_y = reference.dims['x'] # 65
    # reference = np.array(reference.t2m) # this is the optimal version, but for some reason it gives error
    # it should work, it doesn't. Workaround is to
    # This is not a typo, we do not care about the order here
    result = np.zeros_like(forecast)
    n_of_models = np.shape(forecast)[0]
    for k in np.arange(n_of_models):
        model_nr = k
        print("Downscaling completion progress")
        print(k/n_of_models)
        for i in np.arange(n_x):
            # for i in np.arange(1):
            for j in np.arange(n_y):
                # for j in np.arange(10):
                #     print("inner circle")
                # print(j/n_y)
                # ref_dist = np.array(reference.t2m[:,:,i,j])
                # ref_dist = np.sort(ref_dist.flatten())
                ref_dist = np.sort(reference[:, i, j])
                # ref_dist = np.sort(reference[:, i,j])
                frcst_ref_dist = np.sort(frcst_ref[:, i, j])
                ref_ecdf = ECDF(ref_dist)
                frcst_ecdf = ECDF(frcst_ref_dist)
                f_int = interpolate.interp1d(frcst_ecdf.x, frcst_ecdf.y, bounds_error=False, fill_value=(0,1))
                forecast_prob = f_int(forecast[model_nr,:,i,j])
                ref_int = interpolate.interp1d(ref_ecdf.y, ref_ecdf.x, fill_value="extrapolate") # Should we extrapolate?
                result[model_nr, :, i, j] = ref_int(forecast_prob)
    return result

def do_downscaling_parallel_inner(parallel_k, reference, frcst_ref, forecast):
    print(parallel_k)
    n_x = np.shape(reference)[1]
    n_y = np.shape(reference)[2]
    ref_1, ref_2, ref_3, ref_4 = np.shape(forecast)
    result_inner = np.ones((ref_2, ref_3, ref_4))*np.nan
    for i in np.arange(n_x):
        # for i in np.arange(1):
        for j in np.arange(n_y):
            ref_dist = np.sort(reference[:, i, j]) # sorted UERRA distribution, reference period
            frcst_ref_dist = np.sort(frcst_ref[:, i, j]) # sorted forecast distribution, reference period

            # For some reason ECDF counts NAN as part of the distribution,
            # therefore skewing the distribution towards lower values
            ref_dist = ref_dist[~np.isnan(ref_dist)]
            frcst_ref_dist = frcst_ref_dist[~np.isnan(frcst_ref_dist)]
            if len(ref_dist)*len(frcst_ref_dist) >0: # ECDF function will fail if provided with an empty array
                ref_ecdf = ECDF(ref_dist) # empirical CDF from sorted UERRA distribution (reference period)
                frcst_ecdf = ECDF(frcst_ref_dist) # empirical CDF from sorted forecast distribution (reference period)
                f_int = interpolate.interp1d(frcst_ecdf.x, frcst_ecdf.y, bounds_error=False, fill_value=(0,1))
                forecast_prob = f_int(forecast[parallel_k,:,i,j])
                ref_int = interpolate.interp1d(ref_ecdf.y, ref_ecdf.x, fill_value="extrapolate") # Should we extrapolate?
                result_inner[:, i, j] = ref_int(forecast_prob)
    # print("test")
    return result_inner


def do_downscaling_parallel(reference, frcst_ref, forecast, parallel_num):
    # all should be on the same grid
    n_x = np.shape(reference)[1]
    n_y = np.shape(reference)[2]
    # n_x = reference.dims['y'] # 62
    # n_y = reference.dims['x'] # 65
    # reference = np.array(reference.t2m) # this is the optimal version, but for some reason it gives error
    # it should work, it doesn't. Workaround is to
    # This is not a typo, we do not care about the order here
    result = np.zeros_like(forecast)
    n_of_models = np.shape(forecast)[0]

    # This is for testing purposes
    # n_of_models = 4
    result = Parallel(n_jobs=parallel_num)(delayed(do_downscaling_parallel_inner)(k, reference, frcst_ref, forecast) for k in range(n_of_models))
    return result


def seasonal_downscaling_paralel_vs_single_comparison(uerra_dir, uearra_ref_dir, forecast_dir,
                                                      forecast_year, forecast_init_month, forecast_valid_month,
                                                      reference_year_start, reference_year_end, parallel_num):
    # if this function is called, instead of "seasonal_downscaling",
    # then the comparison between parallel and single-threaded versions of seasonal downscaling process is carried out
    # Downscaling are being done to a reference period that can be specified here
    # reference_year_start = 1993
    # reference_year_end = 2018
    # # The forecast that we need to downscale is
    # forecast_year = 2019
    # forecast_init_month = 1
    # forecast_valid_month = 1

    # No changes are needed below this point
    forecast_orig = datetime.datetime(forecast_year, forecast_init_month, 1, 0, 0, 0)
    valid_time_start =  datetime.datetime(forecast_year, forecast_valid_month, 1,0,0,0)
    valid_time_end = datetime.datetime(forecast_year, forecast_valid_month +1, 1,0,0,0)
    delta_start = valid_time_start - forecast_orig
    delta_end = valid_time_end - forecast_orig

    uerra_data = get_reference_data(uearra_ref_dir, reference_year_start, reference_year_end, forecast_valid_month)
    # TODO: This must be changed !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    uerra_data_for_coordinates = get_uerra_data_v2(uerra_dir, reference_year_end, forecast_init_month)
    #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    frcst_ref = get_forecast_reference(forecast_dir, reference_year_start, reference_year_end, forecast_init_month, forecast_valid_month)
    frcst_frcst = get_forecast_data(forecast_dir, forecast_year, forecast_init_month)
    # frcst_ref = frcst_ref.sel(step=slice(delta_start, delta_end))

    frcst_frcst = frcst_frcst.sel(step=slice(delta_start, delta_end)) #
    print("Starting reference forecast interpolation")
    frcst_ref_i = interpolate_to_reference_np(frcst_ref, frcst_frcst.latitude,frcst_frcst.longitude, uerra_data_for_coordinates.latitude, uerra_data_for_coordinates.longitude)
    print("Reference forecast interpolation finished")
    frcst_frcst_i = interpolate_to_reference_xr(frcst_frcst,  frcst_frcst.latitude,frcst_frcst.longitude, uerra_data_for_coordinates.latitude, uerra_data_for_coordinates.longitude)
    print("Forecast interpolation finished")
    print("Starting downscaling")
    # Parallel downscaling
    t_start = time.time()
    result = do_downscaling_parallel(uerra_data, frcst_ref_i, frcst_frcst_i,parallel_num)
    print("Time With parallization: {:.3f}s".format(time.time() - t_start))
    # print(frcst_ref)
    t_start = time.time()
    result = do_downscaling(uerra_data, frcst_ref_i, frcst_frcst_i)
    print("Time Without parallization: {:.3f}s".format(time.time() - t_start))
    print("Downscaling finished")

    print("Downscaling finished")
    # print(frcst_ref)

    # Example plotting
    lon_plot = np.array(uerra_data_for_coordinates.longitude)
    lat_plot = np.array(uerra_data_for_coordinates.latitude)
    # model_nr = 0
    fig = plt.figure(figsize=(9, 6))
    ex_plot_before = np.mean(frcst_frcst_i[:, :, :,:], axis=(0,1)) - 273.16
    uerra_map.pcolormesh(lon_plot, lat_plot, ex_plot_before, latlon=True, shading='auto', cmap=plt.cm.RdBu_r)
    uerra_map.drawcoastlines(linewidth=0.5)
    uerra_map.drawcountries(linewidth=0.5)
    uerra_map.colorbar()
    fig.savefig("forecast_before_ds.png", dpi=300, bbox_inches='tight')
    plt.close(fig)


    fig = plt.figure(figsize=(9, 6))
    ex_plot_after = np.nanmean(result[:, :, :,:], axis=(0,1)) - 273.16
    uerra_map.pcolormesh(lon_plot, lat_plot, ex_plot_after, latlon=True, shading='auto', cmap=plt.cm.RdBu_r)
    uerra_map.drawcoastlines(linewidth=0.5)
    uerra_map.drawcountries(linewidth=0.5)
    uerra_map.colorbar()
    fig.savefig("forecast_after_ds.png", dpi=300, bbox_inches='tight')
    plt.close(fig)


def seasonal_downscaling(uerra_dir, uerra_ref_dir,create_ref, forecast_dir,
                         forecast_year, forecast_init_month, forecast_valid_month,
                         reference_year_start, reference_year_end,
                         poi_lat, poi_lon, output_directory, parallel_num):


    # No changes are needed below this point
    pathlib.Path(output_directory).mkdir(parents=True, exist_ok=True)
    # Let's find the closest coordinates for our point of interest
    uerra_data_for_coordinates = get_uerra_data_v2(uerra_dir, reference_year_end, forecast_init_month)
    lon_plot = np.array(uerra_data_for_coordinates.longitude)
    lat_plot = np.array(uerra_data_for_coordinates.latitude)
    # Let's find closest gridpoint to our poit of interest
    fl_grid_lon = lon_plot.flatten()
    fl_grid_lat = lat_plot.flatten()
    distance_km = np.zeros(np.size(lon_plot))
    for kk in np.arange(len(distance_km)):
        # This needs to be lat, lon order!
        distance_km[kk] = distance.geodesic((fl_grid_lat[kk], fl_grid_lon[kk]),
                                            (poi_lat, poi_lon)).km

    poi_indx_1, poi_indx_2 = np.unravel_index(distance_km.argmin(), lon_plot.shape)

    point_list_forecast = []
    point_list_uerra = []
    point_list_month = []
    point_list_year = []
    list_uerra_years = []
    n_of_months = 7
    # n_of_months = 2
    for delta_month in np.arange(0,n_of_months):
        valid_month = forecast_init_month + delta_month
        forecast_valid_year = forecast_year
        if valid_month > 12:
            valid_month = valid_month - 12
            forecast_valid_year = forecast_year + 1
        if create_ref:
            create_reference_dataset(uerra_dir, uerra_ref_dir, reference_year_start, reference_year_end, valid_month)


        forecast_orig = datetime.datetime(forecast_year, forecast_init_month, 1, 0, 0, 0)
        valid_time_start =  datetime.datetime(forecast_year, valid_month, 1,0,0,0)
        valid_time_end = datetime.datetime(forecast_year, valid_month +1, 1,0,0,0)
        delta_start = valid_time_start - forecast_orig
        delta_end = valid_time_end - forecast_orig

        uerra_data, uerra_years = get_reference_data(uerra_ref_dir, reference_year_start, reference_year_end, valid_month)
        #

        #
        frcst_ref = get_forecast_reference(forecast_dir, reference_year_start, reference_year_end, forecast_init_month, valid_month)
        frcst_frcst = get_forecast_data(forecast_dir, forecast_year, forecast_init_month)
        # frcst_ref = frcst_ref.sel(step=slice(delta_start, delta_end))

        frcst_frcst = frcst_frcst.sel(step=slice(delta_start, delta_end)) #
        print("Starting reference forecast interpolation")
        frcst_ref_i = interpolate_to_reference_np(frcst_ref, frcst_frcst.latitude,frcst_frcst.longitude, uerra_data_for_coordinates.latitude, uerra_data_for_coordinates.longitude)
        print("Reference forecast interpolation finished")
        frcst_frcst_i = interpolate_to_reference_xr(frcst_frcst,  frcst_frcst.latitude,frcst_frcst.longitude, uerra_data_for_coordinates.latitude, uerra_data_for_coordinates.longitude)
        print("Forecast interpolation finished")
        print("Starting downscaling")
        # Parallel downscaling
        t_start = time.time()
        result = do_downscaling_parallel(uerra_data, frcst_ref_i, frcst_frcst_i, parallel_num)
        print("Time With parallization: {:.3f}s".format(time.time() - t_start))
        print(np.shape(result))
        result = np.array(result)
        point_list_forecast.append(result[:,:, poi_indx_1, poi_indx_2])
        point_list_uerra.append(uerra_data[ :,  poi_indx_1, poi_indx_2])
        point_list_year.append(forecast_valid_year)
        point_list_month.append(valid_month)
        list_uerra_years.append(uerra_years)

        # plotting the results
        fig = plt.figure(figsize=(9, 6))
        ex_plot_before = np.mean(frcst_frcst_i[:, :, :,:], axis=(0,1)) - 273.16
        uerra_map.pcolormesh(lon_plot, lat_plot, ex_plot_before, latlon=True, shading='auto', cmap=plt.cm.RdBu_r)
        uerra_map.drawcoastlines(linewidth=0.5)
        uerra_map.drawcountries(linewidth=0.5)
        uerra_map.colorbar()
        plt.title("Monthly mean before DS " + str(forecast_valid_year) + " "+str(valid_month))
        fig.savefig(output_directory + "/" +"forecast_before_ds_" +
                    str(forecast_valid_year) + "_"+str(valid_month) +
                    ".png", dpi=300, bbox_inches='tight')
        plt.close(fig)


        fig = plt.figure(figsize=(9, 6))
        ex_plot_after = np.nanmean(result[:, :, :,:], axis=(0,1)) - 273.16
        uerra_map.pcolormesh(lon_plot, lat_plot, ex_plot_after, latlon=True, shading='auto', cmap=plt.cm.RdBu_r)
        uerra_map.drawcoastlines(linewidth=0.5)
        uerra_map.drawcountries(linewidth=0.5)
        uerra_map.colorbar()
        plt.title("Monthly mean after DS " + str(forecast_valid_year) + " "+str(valid_month))
        fig.savefig(output_directory + "/" +"forecast_after_ds_"+
                    str(forecast_valid_year) + "_"+str(valid_month) +
                    ".png", dpi=300, bbox_inches='tight')
        plt.close(fig)

        fig = plt.figure(figsize=(9, 6))
        uerra_plot = np.nanmean(uerra_data[:,:,:], axis=0) - 273.16
        uerra_map.pcolormesh(lon_plot, lat_plot, uerra_plot, latlon=True, shading='auto', cmap=plt.cm.RdBu_r)
        uerra_map.drawcoastlines(linewidth=0.5)
        uerra_map.drawcountries(linewidth=0.5)
        uerra_map.colorbar()
        plt.title("Climatological average " + str(reference_year_start) + "-" + str(reference_year_end) +
                   " "+str(valid_month))
        fig.savefig(output_directory + "/" +"clima_average_"+
                      str(valid_month) +
                    ".png", dpi=300, bbox_inches='tight')
        plt.close(fig)

        fig = plt.figure(figsize=(9, 6))

        uerra_map.pcolormesh(lon_plot, lat_plot, ex_plot_after - uerra_plot, latlon=True, shading='auto', cmap=plt.cm.RdBu_r)
        uerra_map.drawcoastlines(linewidth=0.5)
        uerra_map.drawcountries(linewidth=0.5)
        uerra_map.colorbar()
        plt.title("Monthly mean with respect to climatological average" + str(forecast_valid_year) + " "+str(valid_month))
        fig.savefig(output_directory + "/" +"forecast_vs_clima_average_"+
                    str(forecast_valid_year) + "_"+str(valid_month) +
                    ".png", dpi=300, bbox_inches='tight')
        plt.close(fig)

    # print("this happens")
    print(np.shape(point_list_month))

    #
    n_categories = 3
    percentiles_cat = np.linspace(0,100, n_categories+1)
    # Let's get UERRA yearly averages in points
    clima_min = np.zeros(( len(point_list_month), 1))
    clima_max = np.zeros((len(point_list_month), 1))
    uerra_temp_cat = np.zeros((len(point_list_month), len(percentiles_cat)))
    for i in np.arange(len(point_list_month)):
        uerra_month = pd.DataFrame(point_list_uerra[i])
        uerra_month["year"] = list_uerra_years[i]
        uerra_month = uerra_month.groupby('year').mean() - 273.16 # uerra_monthly_averages
        clima_min[i] = np.percentile(uerra_month.values, 25)
        clima_max[i] = np.percentile(uerra_month.values, 75)
        # let's get the values for categorical forecasts
        uerra_temp_cat[i, :] = np.percentile(uerra_month.values, percentiles_cat)

    forecast_frame = pd.DataFrame()
    for i in np.arange(len(point_list_month)):
        temp_frame = pd.DataFrame({'means':point_list_forecast[i].mean(axis=1)-273.16}) # 'month': point_list_month[i]
        month_delta = np.ones(len(temp_frame))*i
        temp_frame["month_delta"] = month_delta
        forecast_frame = pd.concat([forecast_frame, temp_frame])
    forecast_frame = forecast_frame.reset_index()
    forecast_frame = forecast_frame.drop(columns = ['index'])
    # print(forecast_frame)
    fig = plt.figure()
    x_for_uerra = np.arange(len(point_list_month))*1.0 # otherwise we get an integer array
    x_for_uerra[0] = x_for_uerra[0] - 0.25 # for the graph to look better
    x_for_uerra[-1] = x_for_uerra[-1] + 0.25
    plt.fill_between(x_for_uerra, np.squeeze(clima_min), np.squeeze(clima_max), label="climatology", color="grey", alpha=0.5)
    ax = sns.boxplot(x="month_delta", y="means", data=forecast_frame, color="blue")
    ax.plot([], [], label='forecast', color='b')  # it is impossible to label all boxplots together, so we plot some nonexistent data
    ax.set_xticklabels(point_list_month)
    ax.set_xlabel("month")
    ax.set_ylabel("Monthly mean, C")
    ax.legend(loc="best")
    fig.savefig(output_directory + "/" +"point_data_"+
                str(forecast_valid_year) + "_"+str(forecast_init_month) +
                ".png", dpi=300, bbox_inches='tight')
    plt.close(fig)

    results_cat = np.zeros((len(point_list_month), n_categories))
    for i in np.arange(len(point_list_month)):
        month_frame = forecast_frame[forecast_frame["month_delta"]==i]
        digi_forecast = np.digitize(month_frame.means, uerra_temp_cat[i, :]) # digitize returns 0 and len(bins) for values that are larger than smallest and largest bins
        for j in np.arange(n_categories):
            results_cat[i,j] = np.count_nonzero(digi_forecast==(j+1))/np.shape(month_frame)[0]*100
        results_cat[i,0] = np.count_nonzero(digi_forecast==0)/np.shape(month_frame)[0]*100 # Add very cold forecasts (colder than climatology) as cold
        results_cat[i,-1] = np.count_nonzero(digi_forecast==n_categories)/np.shape(month_frame)[0]*100 # Add forecasts hotter than climatology to hot forecasts

    fig = plt.figure()
    sns.heatmap(results_cat)
    fig.savefig(output_directory + "/" +"point_data_prob_heatmap"+
                str(forecast_valid_year) + "_"+str(forecast_init_month) +
                ".png", dpi=300, bbox_inches='tight')

    plt.close(fig)

    results_cat = pd.DataFrame(results_cat)
    results_cat.columns = ["Cold", "Normal", "Warm"]
    results_cat = results_cat.stack()
    results_cat = results_cat.reset_index()
    results_cat.columns = ["month", "category", "value"]
    # fig = plt.figure()
    g = sns.catplot(
        data=results_cat, kind="bar",
        x="month", y="value", hue="category", palette="RdYlBu_r")
    # g.set_xticklabels(["Cold", "Normal", "Warm"])
    g.fig.savefig(output_directory + "/" +"point_data_prob_"+
                str(forecast_valid_year) + "_"+str(forecast_init_month) +
                ".png", dpi=300, bbox_inches='tight')
    plt.close(fig)



def create_reference_dataset(uerra_dir, uerra_ref_dir, reference_start_year, reference_end_year, month):


    # where to put the reference dataset
    outputfolder = uerra_ref_dir
    # reference_start_year = 1993
    # reference_end = 2018
    # month = 1
    for year in range(reference_start_year, reference_end_year +1):
        print(year)
        temp = get_uerra_data_v2(uerra_dir, year, month)
        if year==reference_start_year:
            result = temp
        else:
            result = xr.concat((result, temp), dim="time")
    #print(result)
    year = result.time.dt.year
    year = np.array(year)
    year = np.tile(year, (6,1))
    year = np.transpose(year)

    result = np.array(result.t2m)
    sh1, sh2, sh3, sh4 = np.shape(result)
    result = np.reshape(result, (sh1*sh2, sh3, sh4))
    year = np.reshape(year, sh1*sh2)
    # result = np.sort(result, axis=0)
    # let's get year

    filename = "uerra_ref_" + str(reference_start_year) + "_" + str(reference_end_year) + "_" + str(month) + ".npy"
    np.save(outputfolder + filename, result, allow_pickle=True)
    filename_years = "uerra_ref_years" + str(reference_start_year) + "_" + str(reference_end_year) + "_" + str(month) + ".npy"
    np.save(outputfolder + filename_years, year, allow_pickle=True)

def get_reference_data(uerra_ref_dir, start_year, end_year, month):
    inputfolder = uerra_ref_dir
    # inputfolder = "C:/data/seasonal_prediction/data/UERRA_ref/"
    filename = "uerra_ref_" + str(start_year) + "_" + str(end_year) + "_" + str(month) + ".npy"
    arr = np.load(inputfolder + filename, mmap_mode=None)

    filename_years = "uerra_ref_years"  + str(start_year) + "_" + str(end_year) + "_" + str(month) + ".npy"
    years = np.load(inputfolder + filename_years, mmap_mode=None)
    return arr, years


def get_forecast_reference(forecast_dir, start_year, end_year, init_month, valid_month):
    for year in range(start_year, end_year +1):
        print(year)
        orig_time = datetime.datetime(year, init_month, 1, 0, 0, 0)
        valid_time_start =  datetime.datetime(year, valid_month, 1,0,0,0)
        valid_time_end = datetime.datetime(year, valid_month +1, 1,0,0,0)
        delta_start = valid_time_start - orig_time
        delta_end = valid_time_end - orig_time

        temp = get_forecast_data(forecast_dir, year, init_month)
        temp = temp.sel(step=slice(delta_start, delta_end))
        if year==start_year:
            result = temp
        else:
            result = xr.concat((result, temp), dim="time")

    result = np.array(result.t2m)
    sh1, sh2, sh3, sh4, sh5 = np.shape(result)
    result = np.reshape(result, (sh1*sh2*sh3, sh4, sh5))

    result = np.sort(result, axis=0)
    return result

def test():
    print("test")
    point_list_month = [1,2,3,4]
    reference_year_start = 1993
    reference_year_end = 2018
    uerra_ref_dir = "C:/data/seasonal_prediction/data/UERRA_ref/" # directory for saving the UERRA reference (intermediate) results.
    uerra_data, uerra_years = get_reference_data( uerra_ref_dir, reference_year_start, reference_year_end, 1)
    point_list_uerra = []
    list_uerra_years = []
    for i in np.arange(4):
        point_list_uerra.append(uerra_data[:, 30+i, 30+i])
        list_uerra_years.append(uerra_years)
    # Let's get UERRA yearly averages in points
    clima_min = np.zeros(( len(point_list_month), 1))
    clima_max = np.zeros((len(point_list_month), 1))
    for i in np.arange(len(point_list_month)):
        uerra_month = pd.DataFrame(point_list_uerra[i])
        uerra_month["year"] = list_uerra_years[i]
        uerra_month = uerra_month.groupby('year').mean() - 273.16 # uerra_monthly_averages
        clima_min[i] = np.percentile(uerra_month.values, 25)
        clima_max[i] = np.percentile(uerra_month.values, 75)




    with open("point_forecast.txt", 'rb') as f:
        point_list_forecast = pickle.load(f)
    print(np.shape(point_list_uerra))

    forecast_frame = pd.DataFrame()
    for i in np.arange(len(point_list_month)):
        temp_frame = pd.DataFrame({'means':point_list_forecast[i].mean(axis=1)-273.16}) # 'month': point_list_month[i]
        month_delta = np.ones(len(temp_frame))*i
        temp_frame["month_delta"] = month_delta
        forecast_frame = pd.concat([forecast_frame, temp_frame])
    forecast_frame = forecast_frame.reset_index()
    forecast_frame = forecast_frame.drop(columns = ['index'])
    # print(forecast_frame)
    fig = plt.figure()
    x_for_uerra = np.arange(len(point_list_month))*1.0 # otherwise we get an integer array
    x_for_uerra[0] = x_for_uerra[0] - 0.25 # for the graph to look better
    x_for_uerra[-1] = x_for_uerra[-1] + 0.25
    plt.fill_between(x_for_uerra, np.squeeze(clima_min), np.squeeze(clima_max), label="climatology", color="grey", alpha=0.5)
    ax = sns.boxplot(x="month_delta", y="means", data=forecast_frame, color="blue")
    ax.plot([], [], label='forecast', color='b')  # it is impossible to label all boxplots together, so we plot some nonexistent data
    ax.set_xticklabels(point_list_month)
    ax.set_xlabel("month")
    ax.set_ylabel("Monthly mean, C")
    ax.legend(loc="best")
    # plt.plot(x_for_uerra, clima_min)
    # plt.plot(x_for_uerra, clima_max)
    # fig.savefig(output_directory + "/" +"forecast_vs_clima_average_"+
    #             str(forecast_valid_year) + "_"+str(valid_month) +
    #             ".png", dpi=300, bbox_inches='tight')

    plt.close(fig)
    # results_cat = np.zeros((len(point_list_month), len(percentiles_cat)+2))
    results_cat = [[50, 30, 20],[20,20,60],[10, 80, 10],[60, 20,20],[25, 50, 25]]

    results_cat = pd.DataFrame(results_cat)
    results_cat.columns = ["Cold", "Normal", "Warm"]
    results_cat = results_cat.stack()
    results_cat = results_cat.reset_index()
    results_cat.columns = ["month", "category", "value"]
    fig = plt.figure()
    g = sns.catplot(
        data=results_cat, kind="bar",
        x="month", y="value", hue="category", palette="RdYlBu_r")
    # g.set_xticklabels(["Cold", "Normal", "Warm"])
    plt.close(fig)




def test2():
    test = np.ones((1,8))*np.nan
    print(test)
    a = np.array([])
    b = np.array([1,2,3])
    print(len(a))
    print(len(b))
    print(len(a)*len(b))

main_script()
# test2()
# test()
# uerra_dir = "C:/data/seasonal_prediction/data/uerra_ecmwf/" # directory where the UEARRA data can be found
# reference_year_start = 1993
# reference_year_end = 2018
# uerra_ref_dir = "C:/data/seasonal_prediction/data/UERRA_ref/" # directory for saving the UERRA reference (intermediate) results.
# create_reference_dataset(uerra_dir, uerra_ref_dir, reference_year_start, reference_year_end, 12)
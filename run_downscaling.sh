#!/bin/bash
#SBATCH --job-name=downscale # Uzdevuma nosaukums
#SBATCH --partition=regular  #Rindas nosaukums
#SBATCH --ntasks=8     # Kodolu/procesoru skaits
#SBATCH --time=48:00:00 # Laikalimits,h:min:s

module load anaconda3/anaconda-2020.11
source activate seasonal_downscaling
python /home/sile/seasonal_prediction/code/seasonal-prediction/seasonal_prediction.py

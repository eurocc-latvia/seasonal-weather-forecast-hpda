# Seasonal Weather Forecast HPDA

HPC demo-case for the purposes of the project "National Competence Centres in the framework of EuroHPC" (EuroCC)

Seasonal forecasts are weather forecasts made for time periods between one and six months from the moment of forecast creation. Such forecasts are available for free, but they have low spatial resolution.
This HPC use-case deals with downloading a low resolution seasonal forecast from a remote server and then creating a higher resolution forecast using a downscaling procedure that combines the lower resolution forecast with high resolution historical data.

Throughout this use-case it is assumed that the HPC has Anaconda environments available and Slurm is used as the queue management system.

[[_TOC_]]


## Downloading data
### Preparation for downloading data
To download the necessary data from CDS, an account for Copernicus Data Store (CDS) is necessary.

This link contains all the necessary information:
https://cds.climate.copernicus.eu/api-how-to

All the steps (Getting an account, Installing the API key and installing CDS API client) are necessary.

### Installing the download client on HPC (first time)
```
srun --partition=debug --pty bash
module load anaconda3/anaconda-2020.11
conda create -n download_data
source activate download_data
pip install cdsapi
```
### For debugging purposes
```
srun --partition=debug --pty bash 
source activate download_data
```
If it is necessary to deactivate the environment (for instance, to use a different one)
```
conda deactivate 
```
### Alternative source of data
Sometimes, if the data download from CDS is slow, it is better to download UERRA data directly from ECMW.
To download the necessary data from ECMWF, an ECMWF account is necessary. Additionally, a specific file (API key) that is associated with the account must be downloaded.
More information can be found here:
https://www.ecmwf.int/en/forecasts/access-forecasts/ecmwf-web-api

In addition, a Python library "ecmwf-api-client" must be installed on the target system, 
using 
```
pip install ecmwf-api-client
```

More information:
https://confluence.ecmwf.int/display/WEBAPI/Access+ECMWF+Public+Datasets
A different script is needed to download data from ECMWF, it is adapted from examples provided by ECMWF. 

### To download the necessary files, after the system has been set up.

All the downloads are carried out by using python scripts. Each type of data is downloaded by a separate script.  
To download forecast data use: 
```
python download_seasonal_forecast_grib.py
```
To download UERRA data (CDS):
```
python download_uerra.py
```
To download UERRA data (ECMWF):
```
python download_temperature_2m_uearra_ecmwf.py
```
In all of those scripts, a user can change the dates of data being downloaded, the download destination folder, the necessary time interval and the geographical region.

The line of code: 
```
python download_seasonal_forecast_grib.py
```
is the one that runs the download script. Depending on the system used, e.g., if a queue scheduling system is used, it may be necessary to use a more elaborate method of running the script. For instance, on a system with Slurm, it will be necessary instead to call a bash script that ensures submitting the download script to the management system.

Let's look at an example bash script that submits the download script to the queue.

```
#!/bin/bash
#SBATCH --job-name=download_forecast # Job title
#SBATCH --partition=regular  # Queue name
#SBATCH --ntasks=1     # Number of cores
#SBATCH --time=48:00:00 # Time limit,h:min:s

module load anaconda3/anaconda-2020.11
source activate download_data
python /home/sile/seasonal_prediction/code/seasonal-prediction/download_seasonal_forecast_grib.py
```

In this use-case this script is called "download_forecast_script.sh" and can be sumbitted to the queue using
```
sbatch download_forecast_script.sh

```

Let's analyze this script. It gives this job a name that can be seen later looking at the job queue using `squeue`.
The script will submit the job to the "regular" queue with lower computational power and request only one core for the task, as most of the time will be spent waiting on the data anyways. Downloading data can take days or weeks; therefore it is possible that time limit might need to be increased.
Afterwards the script initializes the conda environment and runs the download script.

Similar scripts have been made for other download scripts are available as part of the use-case: `download_uerra_script_ecmwf.sh` and `download_uerra_script.sh`.

# Setting up the environment for downscaling

Let's start by creating a new conda environment called "seasonal_downscaling"

```
module load anaconda3/anaconda-2020.11
conda create -n seasonal_downscaling
source activate seasonal_downscaling
```
Now let's install the necessary packages

```
conda install -c conda-forge scipy numpy pandas xarray  netCDF4  cfgrib Basemap seaborn geopy statsmodels joblib basemap-data-hires
```

# Downscaling

The file `seasonal_prediction.py` contains a function `main_script()` with all the variables that need initialization. The user should set the variables to reflect what forecast is being downscaled (variables `forecast_year`, `forecast_init_month` and `forecast_valid_month`), the time period that is used for reference (variables `reference_year_start` and `reference_year_end`), and the directories where all the necessary files reside. All directory variables should end with "/".

The location of forecast files should be indicated in the variable `forecast_directory`. The reanalysis files should be in the directory `uerra_dir`. A directory for creating and saving the intermediate files `uerra_ref_dir` should also be indicated. Please make sure that you have write access to the directory indicated in `uerra_ref_dir` variable.

Additional information that should be indicated is the latitude and longitude of the point of interest. More specific results for this point will be shown. 

In addition, the number of parallel process for parallelization should alsow be indicated. 


## Submitting the downscaling script to the queue
The downscaling script can be run by executing
```
python seasonal_downscaling.py
```
However, as in with the download scripts, it may be necessary to submit this script to a Slurm queue.
```
#!/bin/bash
#SBATCH --job-name=downscale #  Job title
#SBATCH --partition=regular  #Queue name
#SBATCH --ntasks=8     # Number of cores
#SBATCH --time=48:00:00 # Time limit,h:min:s

module load anaconda3/anaconda-2020.11
source activate seasonal_downscaling
python /home/sile/seasonal_prediction/code/seasonal-prediction/seasonal_prediction.py
```
In this case the job can be also submitted to "power" queue and it uses 8 cores. The number of cores used can be adjusted, but then it needs to be also changed in the script.

# Visualization of the results
All the results are saved as .png files and saved in the folder indicated by the user. There are two types of plots - spatial plots (maps) and plots corresponding to a point of interest indicated by the user.

Let's start with the spatial plots. There are four kinds of spatial plots for each month of the forecast. The first type shows the low resolution forecast before downscaling. The file name follows the pattern `forecast_before_ds_` followed by the year and the month when the forecast is valid. The title of the plot is "Monthly mean before DS", followed by the year and month when the forecast is valid. "DS" here means "downscaling". The color shows the temperature. Higher temperatures are shown in red, lower - in blue. The temperature scale is indicated on the right.

![Python plot output](README_img/forecast_before_ds_2021_7.png)

The second type of spatial plot shows the downscaled forecast.  The file name follows the pattern `forecast_after_ds_` followed by the year and the month when the forecast is valid. The title of the plot is "Monthly mean after DS", followed by the year and month when the forecast is valid. The interpretation of the results is the same as in the previous plot.

![Python plot output](README_img/forecast_after_ds_2021_7.png)

The third type of the plot shows the climatological reference for each month. The filename follows the pattern `clima_average_` followed by the month. The title is `Climatological average`, then the reference period and then the month. The interpretation of the results is the same as in the previous plots.

![Python plot output](README_img/clima_average_7.png)

The fourth type of plot compares the forecasted temperature with the climatological average. The filename follows the pattern `forecast_vs_clima_average_`, followed by the year and month when the forecast is valid. The interpretation of the results is the same as in the previous plot, but this time the temperature difference (in degrees C) is shown. 

![Python plot output](README_img/forecast_vs_clima_average_2021_7.png)

Now let's look at the plots corresponding to specific points. There are two types of plots. 
The first type of plot has the filename convention `point_data_` followed by the year and month when the forecast was initialized. It shows the mean monthly temperatures of the forecast together with the climatological values. Please see the plot below for more in-depth explanation. 

![Python plot output](README_img/point_data_w_explanation.png)

The second type of plot has the filename convention `point_data_prob` followed by the year and month when the forecast was initialized. It shows the probability of each month being among the hottest third, normal third or the coldest third of the monthly means.

![Python plot output](README_img/point_data_prob_2021_5.png)

# How to adapt the use case

## Changing the point of interest
The point of interest can be changed by changing the `poi_lat` and `poi_lon` variables in function `main_script`.
## Different seasonal forecasts are available. 
This example uses ECMWF created seasonal forecast (SEAS-5). Further information about ECMWF SEAS5 forecast and their verification scores can be found here: https://apps.ecmwf.int/webapps/opencharts/?facets=%7B%22Range%22%3A%5B%22Long%20%28Months%29%22%5D%2C%22Type%22%3A%5B%22Forecasts%22%5D%7D 
Other seasonal forecasts are available and the download script can be easily adapted to download those. See https://cds.climate.copernicus.eu/cdsapp#!/dataset/seasonal-original-single-levels for further details.










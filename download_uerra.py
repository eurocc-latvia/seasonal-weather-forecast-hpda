import cdsapi

def download_data():
    c = cdsapi.Client()
    # This is an automated download script that will download a specified interval of data
    # The time interval refers to the months when the forecast is created
    # The script will download the data between the specified months for each specified year
    start_year = 1993
    end_year = 2019
    start_month = 1
    end_month = 12
    # In this folder the files will be downloaded
    # Filenames follow the pattern
    # 'uerra_' + year_str + '_' + month_str + '_.grib'
    outputfolder = '/home/sile/seasonal_prediction/data/uerra/'
    # After this line no more changes are necessary
    years = range(start_year, end_year+1)
    months = range(start_month, end_month+1)

    for year in years:
        for month in months:
            month_str = "{:02d}".format(month)
            year_str = str(year)
            print("Retrieving " + year_str + " " + month_str)
            c.retrieve(
                'reanalysis-uerra-europe-single-levels',
                {
                    'format': 'grib',
                    'origin': 'uerra_harmonie',
                    'month': month_str,
                    'year': year_str,
                    'variable': '2m_temperature',
                    'day': [
                        '01', '02', '03',
                        '04', '05', '06',
                        '07', '08', '09',
                        '10', '11', '12',
                        '13', '14', '15',
                        '16', '17', '18',
                        '19', '20', '21',
                        '22', '23', '24',
                        '25', '26', '27',
                        '28', '29', '30',
                        '31',
                    ],
                    'time': [
                        '00:00', '06:00', '12:00',
                        '18:00',
                    ],


                },
                outputfolder + '/' + 'uerra_' + year_str + '_' + month_str + '_.grib')



download_data()

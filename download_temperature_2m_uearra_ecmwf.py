import calendar
from ecmwfapi import ECMWFDataServer
server = ECMWFDataServer()


def retrieve_uerra_lfpw():
    """
       A function to demonstrate how to iterate efficiently over several years and months etc
       for a particular UERRA request for origin Meteo France.
       Change the variables below to adapt the iteration to your needs.
       You can use the variable 'target' to organise the requested data in files as you wish.
       In the example below the data are organised in files per month.
    """
    yearStart = 2019
    yearEnd   = 2019
    monthStart =  9
    monthEnd   = 12
    outputfolder = '/home/sile/seasonal_prediction/data/uerra_ecmwf/'
    for year in list(range(yearStart, yearEnd + 1)):
        for month in list(range(monthStart, monthEnd + 1)):
            startDate = '%04d%02d%02d' % (year, month, 1)
            numberOfDays = calendar.monthrange(year, month)[1]
            lastDate = '%04d%02d%02d' % (year, month, numberOfDays)
            target_temperature = outputfolder + "/" + "temperature_2m_all_%04d%02d.grb" % (year, month)
            requestDates = (startDate + "/TO/" + lastDate)
            uerra_lfpw_request(requestDates, target_temperature)

def uerra_lfpw_request(requestDates,  target_temperature):
    """
        Origin Meteo France, surface level, analysis fields.
    """

    server.retrieve({
        "class": "ur",
        "stream": "oper",
        "type": "fc",
        "dataset": "uerra",
        "origin" : "eswi",
        "date": requestDates,
        "expver": "prod",
        "levelist": "2",
        "levtype": "sfc",
        "param": "167",
        "target": target_temperature,
        "time": "00:00:00/06:00:00/12:00:00/18:00:00",
        "step": "1/2/3/4/5/6"
    })



if __name__ == '__main__':
    retrieve_uerra_lfpw()

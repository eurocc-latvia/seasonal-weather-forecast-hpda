#!/bin/bash
#SBATCH --job-name=download_forecast # Uzdevuma nosaukums
#SBATCH --partition=regular  #Rindas nosaukums
#SBATCH --ntasks=1     # Kodolu/procesoru skaits
#SBATCH --time=48:00:00 # Laikalimits,h:min:s

module load anaconda3/anaconda-2020.11
source activate download_data
python /home/sile/seasonal_prediction/code/seasonal-prediction/download_seasonal_forecast_grib.py
